$(document).ready(function () {

    /*first tab*/

    const tabs = $('.service-tabs-header-list');
    const tabsItemsContainer = $('.service-tabs-img-text-list');
    const tabsItems = $('.service-tabs-img-text');
    const triangle = $('.triangle')

    tabs.on('click', '.service-tabs-header', function () {

        tabsItemsContainer.css({
            height: tabsItemsContainer.height()
        });

        $('.active').removeClass('active');
        $(this).addClass('active');
        $(tabsItems[$(this).index()]).addClass('active');
        $(triangle[$(this).index()]).addClass('active');

    });


    /*image gallery*/

    $(function ourAmazingWorks() {
        let selectedClass = "";
        let gallery = $('.img-gallery');
        let galleryItem = $('.gallery-item');
        let loadMoreBtn = $('.load-more-btn');
        let galleryItemHeight = $(galleryItem).height();
        let imgActive = document.getElementsByClassName('gallery-img-active').length;

        let galleryLoadHeight = galleryItemHeight * 3;  /*to get 3 row height image gallery*/

        $(gallery).css({'max-height': 'calc(' + galleryLoadHeight + 'px)'});


        $(".our-work-header").click(function () {

            $(gallery).css({'max-height': 'calc(' + galleryLoadHeight + 'px)'});  /*after click heading refresh gallery's height*/

            $(".our-work-header.active").removeClass("active");
            $(this).addClass("active");


            selectedClass = $(this).attr("data-category");

            $(galleryItem).fadeOut().removeClass('gallery-img-active');
            setTimeout(function () {
                $(selectedClass).fadeIn().addClass('gallery-img-active');
            }, 400);

            /* we need special category (gallery-img-active) to get the active class images length. because we get selected class name in '$(".our-work-header").click' function, but we need it in the another function (btn click)*/

            let imageCount = $(selectedClass).length;
            if (imageCount > 12) {
                setTimeout(function () {
                    $(loadMoreBtn).css("display", "block");
                }, 100);
            } else {
                setTimeout(function () {
                    $(loadMoreBtn).css("display", "none");
                }, 100);
            }

        });


        $(loadMoreBtn).click(function () {

            let maxHeight = $(gallery).height();

            maxHeight = maxHeight + maxHeight;
            /* for every time to get the gallery's new hate // if I give it with 'galleryLoadHeight + galleryLoadHeight' it did not work*/

            setTimeout(function () {
                $(gallery).css({'max-height': 'calc(' + maxHeight + 'px)'});
            }, 400);

            if (maxHeight / galleryItemHeight * 4 <= imgActive) {
                /* in every row we have 4 image. So I need to Multiplication to 4 to get the count of images with class 'gallery-img-active' */
                setTimeout(function () {
                    $(loadMoreBtn).css("display", "block");
                }, 400);
            } else {
                setTimeout(function () {
                    $(loadMoreBtn).css("display", "none");
                }, 400);
            }

        });


    });








    $('.gallery-item').hover(function () {
            if ($(this).hasClass('graphic')) {
                $(this).append($("<div class='galley-item-hover'><div class='gallery-item-hover-icons'><a class='gallery-item-hover-icon' href='#'><i class='fas fa-link'></i></a><a class='gallery-item-hover-icon' href='#'><i class='fas fa-search'></i></a></div><div class='gallery-item-info'><p class='xs-bold upp green-text'>creative design</p><p class='xs-regular'>Graphic Design</p></div></div>"));
            }
            if ($(this).hasClass('web')) {
                $(this).append($("<div class='galley-item-hover'><div class='gallery-item-hover-icons'><a class='gallery-item-hover-icon' href='#'><i class='fas fa-link'></i></a><a class='gallery-item-hover-icon' href='#'><i class='fas fa-search'></i></a></div><div class='gallery-item-info'><p class='xs-bold upp green-text'>creative design</p><p class='xs-regular'>Web Design</p></div></div>"));
            }
            if ($(this).hasClass('landing')) {
                $(this).append($("<div class='galley-item-hover'><div class='gallery-item-hover-icons'><a class='gallery-item-hover-icon' href='#'><i class='fas fa-link'></i></a><a class='gallery-item-hover-icon' href='#'><i class='fas fa-search'></i></a></div><div class='gallery-item-info'><p class='xs-bold upp green-text'>creative design</p><p class='xs-regular'>Landing Pages</p></div></div>"));
            }
            if ($(this).hasClass('wordpress')) {
                $(this).append($("<div class='galley-item-hover'><div class='gallery-item-hover-icons'><a class='gallery-item-hover-icon' href='#'><i class='fas fa-link'></i></a><a class='gallery-item-hover-icon' href='#'><i class='fas fa-search'></i></a></div><div class='gallery-item-info'><p class='xs-bold upp green-text'>creative design</p><p class='xs-regular'>Wordpress</p></div></div>"));
            }


            $('.galley-item-hover').animate({
                bottom: 0
            }, 1)
        },
        function () {
            $(this).find(".galley-item-hover").remove();
        }
    );




    /*slider*/

    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        centerMode: true,
        arrows: true,
        focusOnSelect: true,
    });



    $(".slick-slide").css("outline", "none")
    $(".slick-prev").on("click", arrowActive)
    $(".slick-next").on("click", arrowActive)
    $(".slick-slide").on("click", arrowActive)
    $("body").on("mousemove", arrowActive)

    function arrowActive(){
        $(".slick-slide img").removeClass("slider-img-active")
        $(".slick-center img").addClass("slider-img-active")
    }

});

